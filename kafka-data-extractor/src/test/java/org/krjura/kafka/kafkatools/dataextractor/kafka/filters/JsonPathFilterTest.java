package org.krjura.kafka.kafkatools.dataextractor.kafka.filters;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.record.TimestampType;
import org.junit.jupiter.api.Test;
import org.krjura.kafka.kafkatools.dataextractor.ex.AppException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JsonPathFilterTest {

  @Test
  public void whenQueryIsInvalid() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    assertThrows(AppException.class, () -> {
      new JsonPathFilter("$.name");
    });

    assertThrows(AppException.class, () -> {
      new JsonPathFilter("$.name;equals");
    });

    assertThrows(AppException.class, () -> {
      var filter = new JsonPathFilter("$.name;demo;2");
      filter.keep(record);
    });
  }

  @Test
  public void testEquals() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name;equals;tester");
    assertThat(filter.keep(record)).isTrue();
  }

  @Test
  public void testInvalidEquals() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name == 2;equals;tester");

    assertThrows(AppException.class, () -> {
      assertThat(filter.keep(record)).isTrue();
    });
  }

  @Test
  public void testNotEquals() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name;equals;tester1");
    assertThat(filter.keep(record)).isFalse();
  }

  @Test
  public void testContains() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name;contains;test");
    assertThat(filter.keep(record)).isTrue();
  }

  @Test
  public void testInvalidContains() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name == 2;contains;tester");

    assertThrows(AppException.class, () -> {
      assertThat(filter.keep(record)).isTrue();
    });
  }

  @Test
  public void testNotContains() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name;contains;abc");
    assertThat(filter.keep(record)).isFalse();
  }

  @Test
  public void testStartsWith() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name;startsWith;test");
    assertThat(filter.keep(record)).isTrue();
  }

  @Test
  public void testInvalidStartsWith() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name == 2;startsWith;tester");

    assertThrows(AppException.class, () -> {
      assertThat(filter.keep(record)).isTrue();
    });
  }

  @Test
  public void testNotStartsWith() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name;startsWith;abc");
    assertThat(filter.keep(record)).isFalse();
  }

  @Test
  public void testEndsWith() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name;endsWith;er");
    assertThat(filter.keep(record)).isTrue();
  }

  @Test
  public void testInvalidEndsWith() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name == 2;endsWith;tester");

    assertThrows(AppException.class, () -> {
      assertThat(filter.keep(record)).isTrue();
    });
  }

  @Test
  public void testNotEndsWith() {
    ConsumerRecord<String, String> record = generateDefaultRecord();

    var filter = new JsonPathFilter("$.name;endsWith;er2");
    assertThat(filter.keep(record)).isFalse();
  }

  private ConsumerRecord<String, String> generateDefaultRecord() {
    String value = "{\n" +
        "  \"name\": \"tester\",\n" +
        "  \"surname\": \"uat\"\n" +
        "}";

    return new ConsumerRecord<String, String>(
        "demo", 1, 22, 1583170723747L, TimestampType.NO_TIMESTAMP_TYPE,
        ConsumerRecord.NULL_CHECKSUM, ConsumerRecord.NULL_SIZE, ConsumerRecord.NULL_SIZE,
        "key", value
    );
  }
}