package org.krjura.kafka.kafkatools.dataextractor.kafka.filters;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.record.TimestampType;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MessageKeyFilterTest {

  @Test
  public void testSameFilter() {
    var record = new ConsumerRecord<String, String>(
        "demo", 1, 22, 1583170723747L, TimestampType.NO_TIMESTAMP_TYPE,
        ConsumerRecord.NULL_CHECKSUM, ConsumerRecord.NULL_SIZE, ConsumerRecord.NULL_SIZE,
        "key", "value"
    );

    var filter = new MessageKeyFilter("key");
    assertThat(filter.keep(record)).isTrue();
  }

  @Test
  public void testNotSameFilter() {
    var record = new ConsumerRecord<String, String>(
        "demo", 1, 22, 1583170723747L, TimestampType.NO_TIMESTAMP_TYPE,
        ConsumerRecord.NULL_CHECKSUM, ConsumerRecord.NULL_SIZE, ConsumerRecord.NULL_SIZE,
        "not-key", "value"
    );

    var filter = new MessageKeyFilter("key");
    assertThat(filter.keep(record)).isFalse();
  }
}