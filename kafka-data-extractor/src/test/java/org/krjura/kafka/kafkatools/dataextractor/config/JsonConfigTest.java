package org.krjura.kafka.kafkatools.dataextractor.config;

import java.io.FileNotFoundException;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.krjura.kafka.kafkatools.dataextractor.ex.AppException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.assertj.core.api.Assertions.assertThat;

public class JsonConfigTest {

  @Test
  public void whenFileIsNotFound() {
    AppException appException = assertThrows(AppException.class, () -> {
      JsonConfig.load("/tmp/" + UUID.randomUUID().toString());
    });

    assertThat(appException).hasCauseInstanceOf(FileNotFoundException.class);
  }

  @Test
  public void testDefault() {
    var jsonConfig = JsonConfig.load("etc/docs/config-example.json");

    var kafkaProps = jsonConfig.kafkaProps();

    assertThat(kafkaProps.get("bootstrap.servers")).isEqualTo("127.0.0.1:9092");
    assertThat(kafkaProps.get("enable.auto.commit")).isEqualTo(false);
    assertThat(kafkaProps.get("auto.offset.reset")).isEqualTo("none");
    assertThat(kafkaProps.get("max.poll.records")).isEqualTo("100");
    assertThat(kafkaProps.get("key.deserializer")).isEqualTo("org.apache.kafka.common.serialization.StringDeserializer");
    assertThat(kafkaProps.get("value.deserializer")).isEqualTo("org.apache.kafka.common.serialization.StringDeserializer");
    assertThat(kafkaProps.get("allow.auto.create.topics")).isEqualTo(false);
  }
}