package org.krjura.kafka.kafkatools.dataextractor.kafka.utils;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.kafka.kafkatools.dataextractor.kafka.utils.UsageHelper.shouldPrintHelp;

public class UsageHelperTest {

  @Test
  public void testUsage() {
    assertThat(shouldPrintHelp(new String[] {"-h"})).isTrue();
    assertThat(shouldPrintHelp(new String[] {"--help"})).isTrue();
    assertThat(shouldPrintHelp(new String[] {"--topic", "--help"})).isTrue();
    assertThat(shouldPrintHelp(new String[] {"--topic", "-h"})).isTrue();
    assertThat(shouldPrintHelp(new String[] {"--help", "--topic"})).isTrue();
    assertThat(shouldPrintHelp(new String[] {"--help", "-t"})).isTrue();
    assertThat(shouldPrintHelp(new String[] {"--topic"})).isFalse();
    assertThat(shouldPrintHelp(new String[] {"--partition"})).isFalse();
  }
}