package org.krjura.kafka.kafkatools.dataextractor.args;

import org.apache.commons.cli.MissingOptionException;
import org.junit.jupiter.api.Test;
import org.krjura.kafka.kafkatools.dataextractor.ex.AppException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.assertj.core.api.Assertions.assertThat;

public class ArgsConfigTest {

  @Test()
  public void withNoArguments() {
    String[] args = new String[] {};

    var appException = assertThrows(AppException.class, () -> {
      ArgsConfig.load(args);
    });

    assertThat(appException).hasCauseInstanceOf(MissingOptionException.class);
  }

  @Test()
  public void withMandatoryArgumentsInShortForm() {
    String[] args = new String[] {"-t", "demo", "-o", "demo.json"};

    var config = ArgsConfig.load(args);
    assertThat(config.topicName()).isEqualTo("demo");
    assertThat(config.outputFilename()).isEqualTo("demo.json");

    assertThat(config.hasConfig()).isFalse();
    assertThat(config.hasFromTimestamp()).isFalse();
    assertThat(config.hasKey()).isFalse();
    assertThat(config.hasFilter()).isFalse();
  }

  @Test()
  public void withMandatoryArgumentsInLongForm() {
    String[] args = new String[] {"--topic", "demo", "--output", "demo.json"};

    var config = ArgsConfig.load(args);
    assertThat(config.topicName()).isEqualTo("demo");
    assertThat(config.outputFilename()).isEqualTo("demo.json");

    assertThat(config.hasConfig()).isFalse();
    assertThat(config.hasFromTimestamp()).isFalse();
    assertThat(config.hasKey()).isFalse();
    assertThat(config.hasFilter()).isFalse();
  }

  @Test()
  public void withAllArgumentsInLongForm() {
    String[] args = new String[] {
        "--config", "config.json",
        "--topic", "demo",
        "--output", "demo.json",
        "--partition", "1",
        "--from", "2007-01-01T00:00:00+01:00",
        "--to", "2007-01-01T01:00:00+01:00",
        "--key", "messageKey",
        "--filter", "$.message.key;equals;true"
    };

    var config = ArgsConfig.load(args);
    assertThat(config.config()).isEqualTo("config.json");
    assertThat(config.topicName()).isEqualTo("demo");
    assertThat(config.outputFilename()).isEqualTo("demo.json");
    assertThat(config.getPartitionId()).isPresent();
    assertThat(config.getPartitionId().get()).isEqualTo(1);
    assertThat(config.fromTimestamp()).isPresent();
    assertThat(config.fromTimestamp().get().toInstant().toEpochMilli()).isEqualTo(1167606000000L);
    assertThat(config.toTimestamp()).isPresent();
    assertThat(config.toTimestamp().get().toInstant().toEpochMilli()).isEqualTo(1167609600000L);
    assertThat(config.key()).isEqualTo("messageKey");
    assertThat(config.filter()).hasSize(1);
    assertThat(config.filter()).contains("$.message.key;equals;true");
  }

  @Test()
  public void withAllArgumentsInShortForm() {
    String[] args = new String[] {
        "-c", "config.json",
        "-t", "demo",
        "-o", "demo.json",
        "-p", "1",
        "--from", "2007-01-01T00:00:00+01:00",
        "--to", "2007-01-01T01:00:00+01:00",
        "-k", "messageKey",
        "-f", "$.message.key;equals;true"
    };

    var config = ArgsConfig.load(args);

    assertThat(config.hasConfig()).isTrue();
    assertThat(config.config()).isEqualTo("config.json");
    assertThat(config.topicName()).isEqualTo("demo");
    assertThat(config.outputFilename()).isEqualTo("demo.json");
    assertThat(config.getPartitionId()).isPresent();
    assertThat(config.getPartitionId().get()).isEqualTo(1);
    assertThat(config.hasFromTimestamp()).isTrue();
    assertThat(config.fromTimestamp()).isPresent();
    assertThat(config.fromTimestamp().get().toInstant().toEpochMilli()).isEqualTo(1167606000000L);
    assertThat(config.toTimestamp()).isPresent();
    assertThat(config.toTimestamp().get().toInstant().toEpochMilli()).isEqualTo(1167609600000L);
    assertThat(config.hasKey()).isTrue();
    assertThat(config.key()).isEqualTo("messageKey");
    assertThat(config.hasFilter()).isTrue();
    assertThat(config.filter()).hasSize(1);
    assertThat(config.filter()).contains("$.message.key;equals;true");
  }

  @Test()
  public void withMultipleFiltersInShortForm() {
    String[] args = new String[] {
        "-t", "demo",
        "-o", "demo.json",
        "-f", "$.message.key;equals;true",
        "-f", "$.message.value;equals;false"
    };

    var config = ArgsConfig.load(args);

    assertThat(config.topicName()).isEqualTo("demo");
    assertThat(config.outputFilename()).isEqualTo("demo.json");
    assertThat(config.hasFilter()).isTrue();
    assertThat(config.filter()).hasSize(2);
    assertThat(config.filter()).contains("$.message.key;equals;true");
    assertThat(config.filter()).contains("$.message.value;equals;false");
  }

  @Test()
  public void withMultipleFiltersInLongForm() {
    String[] args = new String[] {
        "--topic", "demo",
        "--output", "demo.json",
        "--filter", "$.message.key;equals;true",
        "--filter", "$.message.value;equals;false"
    };

    var config = ArgsConfig.load(args);

    assertThat(config.topicName()).isEqualTo("demo");
    assertThat(config.outputFilename()).isEqualTo("demo.json");
    assertThat(config.hasFilter()).isTrue();
    assertThat(config.filter()).hasSize(2);
    assertThat(config.filter()).contains("$.message.key;equals;true");
    assertThat(config.filter()).contains("$.message.value;equals;false");
  }
}