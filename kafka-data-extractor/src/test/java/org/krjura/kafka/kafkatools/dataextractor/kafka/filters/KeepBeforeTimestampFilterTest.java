package org.krjura.kafka.kafkatools.dataextractor.kafka.filters;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.record.TimestampType;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class KeepBeforeTimestampFilterTest {

  @Test
  public void testBeforeFilter() {
    var record = new ConsumerRecord<String, String>(
        "demo", 1, 22, 1583170723747L, TimestampType.NO_TIMESTAMP_TYPE,
        ConsumerRecord.NULL_CHECKSUM, ConsumerRecord.NULL_SIZE, ConsumerRecord.NULL_SIZE,
        "key", "value"
    );

    var filter = new KeepBeforeTimestampFilter(1583170723748L);
    assertThat(filter.keep(record)).isTrue();
  }

  @Test
  public void testAfterFilter() {
    var record = new ConsumerRecord<String, String>(
        "demo", 1, 22, 1583170723749L, TimestampType.NO_TIMESTAMP_TYPE,
        ConsumerRecord.NULL_CHECKSUM, ConsumerRecord.NULL_SIZE, ConsumerRecord.NULL_SIZE,
        "key", "value"
    );

    var filter = new KeepBeforeTimestampFilter(1583170723748L);
    assertThat(filter.keep(record)).isFalse();
  }

  @Test
  public void testSameFilter() {
    var record = new ConsumerRecord<String, String>(
        "demo", 1, 22, 1583170723748L, TimestampType.NO_TIMESTAMP_TYPE,
        ConsumerRecord.NULL_CHECKSUM, ConsumerRecord.NULL_SIZE, ConsumerRecord.NULL_SIZE,
        "key", "value"
    );

    var filter = new KeepBeforeTimestampFilter(1583170723748L);
    assertThat(filter.keep(record)).isTrue();
  }

}