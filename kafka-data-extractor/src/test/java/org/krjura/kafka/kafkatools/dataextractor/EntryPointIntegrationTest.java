package org.krjura.kafka.kafkatools.dataextractor;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
public class EntryPointIntegrationTest {

  private static final Logger logger = LoggerFactory.getLogger(EntryPointIntegrationTest.class);

  public static final String CONST_TOPIC_NAME = "testing";
  public static final String CONST_DEFAULT_CONFIG = "/tmp/" + UUID.randomUUID().toString() + ".json";
  public static final String CONST_DEFAULT_OUTPUT = "/tmp/" + UUID.randomUUID().toString() + ".json";

  @Container
  private static final KafkaContainer KAFKA_CONTAINER = new KafkaContainer("5.4.0");

  @BeforeAll
  public static void beforeAll() throws Exception {
    // setup kafka and data first since deleting/creating topics and data is to complicated
    setupKafka();
    writeDefaultConfig();
    setupRecords();
  }

  @Test
  public void mustOutputJsonPathMessages() throws Exception {
    String[] args = new String[] {
        "--topic", CONST_TOPIC_NAME,
        "--output", CONST_DEFAULT_OUTPUT,
        "--config", CONST_DEFAULT_CONFIG,
        "--filter", "$.message;equals;json",
    };

    EntryPoint.main(args);

    var testOutput = Files.readString(Path.of(CONST_DEFAULT_OUTPUT));
    var testExpected = Files.readString(Path.of("etc/testing/test_cases/entry_point_test_must_output_json_path_messages.json"));

    assertThat(testOutput).isEqualTo(testExpected);
  }

  @Test
  public void mustOutputMultipleJsonPathMessages() throws Exception {
    String[] args = new String[] {
        "--topic", CONST_TOPIC_NAME,
        "--output", CONST_DEFAULT_OUTPUT,
        "--config", CONST_DEFAULT_CONFIG,
        "--filter", "$.message;equals;json",
        "--filter", "$.type;equals;account",
    };

    EntryPoint.main(args);

    var testOutput = Files.readString(Path.of(CONST_DEFAULT_OUTPUT));
    var testExpected = Files.readString(Path.of("etc/testing/test_cases/entry_point_test_must_output_json_path_messages.json"));

    assertThat(testOutput).isEqualTo(testExpected);
  }

  @Test
  public void mustNotOutputJsonPathMessagesWhenSecondConditionDoesNotMatch() throws Exception {
    String[] args = new String[] {
        "--topic", CONST_TOPIC_NAME,
        "--output", CONST_DEFAULT_OUTPUT,
        "--config", CONST_DEFAULT_CONFIG,
        "--filter", "$.message;equals;json",
        "--filter", "$.type;equals;order",
    };

    EntryPoint.main(args);

    var testOutput = Files.readString(Path.of(CONST_DEFAULT_OUTPUT));

    assertThat(testOutput).isEqualTo("");
  }

  @Test
  public void mustOutputFromAndToMessages() throws Exception {
    String[] args = new String[] {
        "--topic", CONST_TOPIC_NAME,
        "--output", CONST_DEFAULT_OUTPUT,
        "--config", CONST_DEFAULT_CONFIG,
        "--from", "2020-03-04T10:16:14.500+01:00",
        "--to", "2020-03-04T10:16:15.500+01:00"
    };

    EntryPoint.main(args);

    var testOutput = Files.readString(Path.of(CONST_DEFAULT_OUTPUT));
    var testExpected = Files.readString(Path.of("etc/testing/test_cases/entry_point_test_must_output_from_and_to_messages.json"));

    assertThat(testOutput).isEqualTo(testExpected);
  }

  @Test
  public void mustOutputFromMessages() throws Exception {
    String[] args = new String[] {
        "--topic", CONST_TOPIC_NAME,
        "--output", CONST_DEFAULT_OUTPUT,
        "--config", CONST_DEFAULT_CONFIG,
        "--from", "2020-03-04T10:16:14.500+01:00"
    };

    EntryPoint.main(args);

    var testOutput = Files.readString(Path.of(CONST_DEFAULT_OUTPUT));
    var testExpected = Files.readString(Path.of("etc/testing/test_cases/entry_point_test_must_output_from_messages.json"));

    assertThat(testOutput).isEqualTo(testExpected);
  }

  @Test
  public void mustOutputSingleKeyMessages() throws Exception {
    String[] args = new String[] {
        "--topic", CONST_TOPIC_NAME,
        "--output", CONST_DEFAULT_OUTPUT,
        "--config", CONST_DEFAULT_CONFIG,
        "--key", "3"
    };

    EntryPoint.main(args);

    var testOutput = Files.readString(Path.of(CONST_DEFAULT_OUTPUT));
    var testExpected = Files.readString(Path.of("etc/testing/test_cases/entry_point_test_must_output_single_key_messages.json"));

    assertThat(testOutput).isEqualTo(testExpected);
  }

  @Test
  public void mustOutputPartitionOneMessages() throws Exception {
    String[] args = new String[] {
        "--topic", CONST_TOPIC_NAME,
        "--output", CONST_DEFAULT_OUTPUT,
        "--config", CONST_DEFAULT_CONFIG,
        "--partition", "1"
    };

    EntryPoint.main(args);

    var testOutput = Files.readString(Path.of(CONST_DEFAULT_OUTPUT));
    var testExpected = Files.readString(Path.of("etc/testing/test_cases/entry_point_test_must_output_partition_one_messages.json"));

    assertThat(testOutput).isEqualTo(testExpected);
  }

  @Test
  public void mustOutputAllMessages() throws Exception {
    String[] args = new String[] {
        "--topic", CONST_TOPIC_NAME,
        "--output", CONST_DEFAULT_OUTPUT,
        "--config", CONST_DEFAULT_CONFIG
    };

    EntryPoint.main(args);

    var testOutput = Files.readString(Path.of(CONST_DEFAULT_OUTPUT));
    var testExpected = Files.readString(Path.of("etc/testing/test_cases/entry_point_test_must_output_all_messages.json"));

    assertThat(testOutput).isEqualTo(testExpected);
  }

  private static void setupKafka() throws Exception {

    Map<String, Object> kafkaConfig = new HashMap<>();
    kafkaConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_CONTAINER.getBootstrapServers());

    try(var adminClient = KafkaAdminClient.create(kafkaConfig)) {

      logger.info("creating topic {}", CONST_TOPIC_NAME);
      adminClient.createTopics(List.of(new NewTopic(CONST_TOPIC_NAME, 3, (short) 1))).all().get();
    }
  }

  private static void setupRecords() throws Exception{
    Map<String, Object> kafkaConfig = new HashMap<>();
    kafkaConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_CONTAINER.getBootstrapServers());
    kafkaConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
    kafkaConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

    try(var kafkaProducer = new KafkaProducer<String, String>(kafkaConfig)) {

      kafkaProducer.send(new ProducerRecord<>(CONST_TOPIC_NAME, 0, 1583313374000L, "1", "message 1")).get();
      kafkaProducer.send(new ProducerRecord<>(CONST_TOPIC_NAME, 1, 1583313375000L, "2", "message 2")).get();
      kafkaProducer.send(new ProducerRecord<>(CONST_TOPIC_NAME, 2, 1583313376000L, "3", "{\"message\": \"json\", \"type\": \"account\"}")).get();
    }
  }

  private static void writeDefaultConfig() throws Exception {
    String config = "{\n" +
        "  \"kafka.props\": {\n" +
        "    \"bootstrap.servers\": \"" + KAFKA_CONTAINER.getBootstrapServers() + "\",\n" +
        "    \"enable.auto.commit\": false,\n" +
        "    \"auto.offset.reset\": \"none\",\n" +
        "    \"max.poll.records\": \"100\",\n" +
        "    \"key.deserializer\": \"org.apache.kafka.common.serialization.StringDeserializer\",\n" +
        "    \"value.deserializer\": \"org.apache.kafka.common.serialization.StringDeserializer\",\n" +
        "    \"allow.auto.create.topics\": false\n" +
        "  }\n" +
        "}";

    Files.write(Path.of(CONST_DEFAULT_CONFIG), config.getBytes());
  }
}