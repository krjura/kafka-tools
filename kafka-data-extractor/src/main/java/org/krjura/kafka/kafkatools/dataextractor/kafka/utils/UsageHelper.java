package org.krjura.kafka.kafkatools.dataextractor.kafka.utils;

import static java.util.Objects.requireNonNull;

public final class UsageHelper {

  private UsageHelper() {
    // util
  }

  public static boolean shouldPrintHelp(String[] args) {
    requireNonNull(args);

    for(String arg : args) {
      if(arg.equals("-h") || arg.equals("--help")) {
        return true;
      }
    }

    return false;
  }
}
