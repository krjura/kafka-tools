package org.krjura.kafka.kafkatools.dataextractor.kafka.filters;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface Filter {

  boolean keep(ConsumerRecord<String, String> record);

}
