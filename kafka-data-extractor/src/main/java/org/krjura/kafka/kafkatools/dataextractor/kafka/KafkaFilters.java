package org.krjura.kafka.kafkatools.dataextractor.kafka;

import java.util.List;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.common.TopicPartition;
import org.krjura.kafka.kafkatools.dataextractor.args.ArgsConfig;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

public final class KafkaFilters {

  private KafkaFilters() {
    // util
  }

  public static List<TopicPartition> filterPartitions(
      final ArgsConfig argsConfig, final TopicDescription topicDescription) {

    requireNonNull(argsConfig);
    requireNonNull(topicDescription);

    var topicName = argsConfig.topicName();
    var partitionIdOptional = argsConfig.getPartitionId();

    // if partition id is present filter only data from that partition
    if(partitionIdOptional.isPresent()) {
      return List.of(new TopicPartition(topicName, partitionIdOptional.get()));
      // filter all
    } else {
      return topicDescription
          .partitions()
          .stream()
          .map(tpi -> new TopicPartition(topicName, tpi.partition()))
          .collect(toList());
    }
  }
}
