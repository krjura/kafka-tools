package org.krjura.kafka.kafkatools.dataextractor.kafka.utils;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.common.TopicPartition;

import static java.util.Objects.requireNonNull;

public final class KafkaSeekUtils {

  private KafkaSeekUtils() {
    // util
  }

  public static void seekToBeginning(
      final List<TopicPartition> topicPartitions, final KafkaConsumer<String, String> consumer) {

    requireNonNull(topicPartitions);
    requireNonNull(consumer);

    consumer.seekToBeginning(topicPartitions);
  }

  public static void seekToTimestamp(
      final List<TopicPartition> topicPartitions,
      final ZonedDateTime from,
      final KafkaConsumer<String, String> consumer) {

    requireNonNull(topicPartitions);
    requireNonNull(from);
    requireNonNull(consumer);

    var partitionTimestampMap = toPartitionTimestampMap(topicPartitions, from);
    var partitionOffsetMap = consumer.offsetsForTimes(partitionTimestampMap);

    partitionOffsetMap
        .forEach((tp, offsetAndTimestamp) -> seekToTimestamp(consumer, tp, offsetAndTimestamp));
  }

  public static void seekToTimestamp(
      final KafkaConsumer<String, String> consumer,
      final TopicPartition tp,
      final OffsetAndTimestamp offsetAndTimestamp) {

    requireNonNull(tp);
    requireNonNull(consumer);

    if(offsetAndTimestamp == null) {
      consumer.seekToEnd(List.of(tp));
    } else {
      consumer.seek(tp, offsetAndTimestamp.offset());
    }
  }

  public static Map<TopicPartition, Long> toPartitionTimestampMap(
      final List<TopicPartition> topicPartitions, final ZonedDateTime from) {

    requireNonNull(topicPartitions);
    requireNonNull(from);

    long timestamp = from.toInstant().toEpochMilli();

    return topicPartitions
        .stream()
        .collect(Collectors.toMap(tp -> tp, tp -> timestamp));
  }
}
