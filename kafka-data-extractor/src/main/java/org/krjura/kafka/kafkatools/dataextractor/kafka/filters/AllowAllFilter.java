package org.krjura.kafka.kafkatools.dataextractor.kafka.filters;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public final class AllowAllFilter implements Filter {

  @Override
  public boolean keep(final ConsumerRecord<String, String> record) {
    return true;
  }
}
