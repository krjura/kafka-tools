package org.krjura.kafka.kafkatools.dataextractor.kafka.filters;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import static java.util.Objects.requireNonNull;

public final class MessageKeyFilter implements Filter {

  private final String key;

  public MessageKeyFilter(final String key) {
    this.key = requireNonNull(key);
  }

  @Override
  public boolean keep(final ConsumerRecord<String, String> record) {
    return record.key() == null || record.key().equals(key);
  }
}
