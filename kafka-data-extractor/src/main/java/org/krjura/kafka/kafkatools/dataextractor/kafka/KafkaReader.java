package org.krjura.kafka.kafkatools.dataextractor.kafka;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.List;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.krjura.kafka.kafkatools.dataextractor.args.ArgsConfig;
import org.krjura.kafka.kafkatools.dataextractor.config.JsonConfig;
import org.krjura.kafka.kafkatools.dataextractor.ex.AppException;
import org.krjura.kafka.kafkatools.dataextractor.kafka.filters.Filter;
import org.krjura.kafka.kafkatools.dataextractor.writer.KafkaRecordWriter;

import static java.util.Objects.requireNonNull;
import static org.krjura.kafka.kafkatools.dataextractor.kafka.filters.CompositeFilter.compositeFilter;
import static org.krjura.kafka.kafkatools.dataextractor.kafka.filters.FilterFactory.allowAllFilter;
import static org.krjura.kafka.kafkatools.dataextractor.kafka.filters.FilterFactory.toTimestampFilter;
import static org.krjura.kafka.kafkatools.dataextractor.kafka.utils.KafkaSeekUtils.seekToBeginning;
import static org.krjura.kafka.kafkatools.dataextractor.kafka.utils.KafkaSeekUtils.seekToTimestamp;
import static org.krjura.kafka.kafkatools.dataextractor.writer.KafkaRecordWriter.createKafkaRecordWriter;

public final class KafkaReader {

  private final JsonConfig jsonConfig;

  private final ArgsConfig argsConfig;

  public KafkaReader(JsonConfig jsonConfig, ArgsConfig argsConfig) {
    this.jsonConfig = requireNonNull(jsonConfig);
    this.argsConfig = requireNonNull(argsConfig);
  }

  public void readFromTheBeginning(final List<TopicPartition> topicPartitions, final Filter filter) {
    requireNonNull(topicPartitions);

    try(var writer = createKafkaRecordWriter(argsConfig.outputFilename());
        var consumer = new KafkaConsumer<String, String>(jsonConfig.kafkaProps())) {

      assignPartitions(topicPartitions, consumer);
      seekToBeginning(topicPartitions, consumer);

      readAndWriteData(writer, consumer, compositeFilter(List.of(filter, allowAllFilter())));
    } catch (AppException e) {
      throw e;
    } catch (Exception e) {
      throw new AppException("unknown error occurred");
    }
  }

  public void readFromTimestamp(
      final List<TopicPartition> topicPartitions,
      final ZonedDateTime from,
      final ZonedDateTime to,
      final Filter filter) {

    requireNonNull(topicPartitions);
    requireNonNull(from);

    try(final var writer = createKafkaRecordWriter(argsConfig.outputFilename());
        final var consumer = new KafkaConsumer<String, String>(jsonConfig.kafkaProps())) {

      assignPartitions(topicPartitions, consumer);
      seekToTimestamp(topicPartitions, from, consumer);

      readAndWriteData(writer, consumer, compositeFilter(List.of(filter, toTimestampFilter(to))));
    } catch (AppException e) {
      throw e;
    } catch (Exception e) {
      throw new AppException("unknown error occurred", e);
    }
  }

  private void assignPartitions(
      final List<TopicPartition> topicPartitions, final KafkaConsumer<String, String> consumer) {

    consumer.assign(topicPartitions);
  }

  private void readAndWriteData(
      final KafkaRecordWriter writer, final KafkaConsumer<String, String> consumer, final Filter filter) {

    while ( true ) {
      var records = consumer.poll(Duration.ofSeconds(1));

      if(records.count() == 0) {
        break;
      }

      for (var record : records) {

        if(filter.keep(record)) {
          writer.writeRecord(record);
        }
      }
    }
  }
}
