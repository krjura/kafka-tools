package org.krjura.kafka.kafkatools.dataextractor;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.krjura.kafka.kafkatools.dataextractor.args.ArgsConfig;
import org.krjura.kafka.kafkatools.dataextractor.config.JsonConfig;
import org.krjura.kafka.kafkatools.dataextractor.ex.AppException;
import org.krjura.kafka.kafkatools.dataextractor.kafka.KafkaAdminSupport;
import org.krjura.kafka.kafkatools.dataextractor.kafka.KafkaReader;
import org.krjura.kafka.kafkatools.dataextractor.kafka.filters.Filter;
import org.krjura.kafka.kafkatools.dataextractor.kafka.filters.JsonPathFilter;
import org.krjura.kafka.kafkatools.dataextractor.kafka.filters.MessageKeyFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.krjura.kafka.kafkatools.dataextractor.kafka.KafkaFilters.filterPartitions;
import static org.krjura.kafka.kafkatools.dataextractor.kafka.filters.CompositeFilter.compositeFilter;
import static org.krjura.kafka.kafkatools.dataextractor.kafka.utils.UsageHelper.shouldPrintHelp;

public final class EntryPoint {

  private static final Logger logger = LoggerFactory.getLogger(EntryPoint.class);

  public void run(String[] args) {
    var arguments = ArgsConfig.load(args);
    var config = JsonConfig.load(arguments.config());

    var adminSupport = new KafkaAdminSupport(config, arguments);
    var topicDescription = adminSupport.topicDescription();

    var topicPartitions = filterPartitions(arguments, topicDescription);

    var kafkaReader = new KafkaReader(config, arguments);

    var filters = buildFilters(arguments);

    if(arguments.hasFromTimestamp()) {
      ZonedDateTime from = arguments
          .fromTimestamp().orElseThrow(() -> new AppException("from is not defined"));
      ZonedDateTime to = arguments.toTimestamp().orElse(null);

      kafkaReader.readFromTimestamp(topicPartitions, from, to, filters);
    } else {
      kafkaReader.readFromTheBeginning(topicPartitions, filters);
    }
  }

  private Filter buildFilters(ArgsConfig arguments) {
    List<Filter> filters = new ArrayList<>();

    if(arguments.hasKey()) {
      filters.add(new MessageKeyFilter(arguments.key()));
    }

    if(arguments.hasFilter()) {
      Stream.of(arguments.filter())
          .map(JsonPathFilter::new)
          .collect(Collectors.toList())
          .forEach(filters::add);
    }

     return compositeFilter(filters);
  }

  private static void printHelp() {
    ArgsConfig.outputHelp();
  }

  public static void main(String[] args) {
    if(shouldPrintHelp(args)) {
      printHelp();
      return;
    }

    try {
      var entryPoint = new EntryPoint();
      entryPoint.run(args);
    } catch (Exception e) {
      if(logger.isDebugEnabled()) {
        logger.warn("fetching data from kafka failed", e);
      } else {
        logger.warn(e.getMessage());
      }
    }
  }
}
