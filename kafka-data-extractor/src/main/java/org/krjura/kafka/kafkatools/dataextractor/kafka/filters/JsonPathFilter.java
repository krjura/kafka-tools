package org.krjura.kafka.kafkatools.dataextractor.kafka.filters;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.krjura.kafka.kafkatools.dataextractor.ex.AppException;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.CoreMatchers.endsWith;

public final class JsonPathFilter implements Filter {

  private final String query;

  private String expression;

  private String operation;

  private String value;

  public JsonPathFilter(final String query) {
    this.query = requireNonNull(query);

    parseQuery();
  }

  private void parseQuery() {
    String[] parts = this.query.split(";");

    if(parts.length != 3) {
      throw new AppException(
          "cannot parse JSON path expression. Expecting expression;operation;value but got " + this.query);
    }

    this.expression = parts[0].trim();
    this.operation = parts[1].trim();
    this.value = parts[2].trim();
  }

  @Override
  public boolean keep(final ConsumerRecord<String, String> record) {
    switch (operation) {
      case "equals":
        return executeEquals(record);
      case "contains":
        return executeContains(record);
      case "startsWith":
        return executeStartsWith(record);
      case "endsWith":
        return executeEndsWith(record);
      default:
        throw new AppException("unknown JSON path operation " + operation);
    }
  }

  private boolean executeEquals(final ConsumerRecord<String, String> record) {
    try {
      return hasJsonPath(expression, equalTo(value)).matches(record.value());
    } catch (Exception e) {
      throw new AppException("cannot execute JSON path query: " + e.getMessage());
    }
  }

  private boolean executeContains(final ConsumerRecord<String, String> record) {
    try {
      return hasJsonPath(expression, containsString(value)).matches(record.value());
    } catch (Exception e) {
      throw new AppException("cannot execute JSON path query: " + e.getMessage());
    }
  }

  private boolean executeStartsWith(final ConsumerRecord<String, String> record) {
    try {
      return hasJsonPath(expression, startsWith(value)).matches(record.value());
    } catch (Exception e) {
      throw new AppException("cannot execute JSON path query: " + e.getMessage());
    }
  }

  private boolean executeEndsWith(final ConsumerRecord<String, String> record) {
    try {
      return hasJsonPath(expression, endsWith(value)).matches(record.value());
    } catch (Exception e) {
      throw new AppException("cannot execute JSON path query: " + e.getMessage());
    }
  }
}
