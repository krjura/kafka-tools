package org.krjura.kafka.kafkatools.dataextractor.kafka;

import java.util.List;
import java.util.concurrent.ExecutionException;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.TopicDescription;
import org.krjura.kafka.kafkatools.dataextractor.args.ArgsConfig;
import org.krjura.kafka.kafkatools.dataextractor.config.JsonConfig;
import org.krjura.kafka.kafkatools.dataextractor.ex.AppException;

import static java.util.Objects.requireNonNull;

public final class KafkaAdminSupport {

  private final JsonConfig jsonConfig;

  private final ArgsConfig argsConfig;

  public KafkaAdminSupport(final JsonConfig jsonConfig,  final ArgsConfig argsConfig) {
    this.jsonConfig = requireNonNull(jsonConfig);
    this.argsConfig = requireNonNull(argsConfig);
  }

  public TopicDescription topicDescription() {

    try(final var adminClient = AdminClient.create(jsonConfig.kafkaProps())) {

      var topicName = argsConfig.topicName();

      var topics = adminClient.describeTopics(List.of(topicName)).all().get();

      return topics.get(topicName);
    } catch (InterruptedException | ExecutionException e) {
      throw new AppException("Cannot query with kafka admin client: " + e.getMessage(), e);
    }
  }
}
