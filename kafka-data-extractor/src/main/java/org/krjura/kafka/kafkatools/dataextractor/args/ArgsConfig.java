package org.krjura.kafka.kafkatools.dataextractor.args;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.krjura.kafka.kafkatools.dataextractor.ex.AppException;

import static java.util.Objects.requireNonNull;

public final class ArgsConfig {

  private static final CommandLineParser parser = new DefaultParser();

  private static final Option CONFIG_OPTION = Option
      .builder("c")
      .longOpt("config")
      .desc("Location of the configuration file")
      .hasArg()
      .type(String.class)
      .build();

  private static final Option TOPIC_OPTION = Option
      .builder("t")
      .longOpt("topic")
      .desc("Name of the topic")
      .required()
      .hasArg()
      .type(String.class)
      .build();

  private static final Option PARTITION_OPTION = Option
      .builder("p")
      .longOpt("partition")
      .desc("Id of the partition")
      .hasArg()
      .type(Integer.class)
      .build();

  private static final Option OUT_FILE_OPTION = Option
      .builder("o")
      .longOpt("output")
      .desc("Filename where to store the result")
      .hasArg()
      .required()
      .type(String.class)
      .build();

  private static final Option FROM_TIMESTAMP_OPTION = Option
      .builder("from")
      .longOpt("from")
      .desc("Timestamp of the first entry in ISO-8601 format e.g. 2007-12-03T10:15:30+01:00")
      .hasArg()
      .type(String.class)
      .build();

  private static final Option TO_TIMESTAMP_OPTION = Option
      .builder("to")
      .longOpt("to")
      .desc("Timestamp of the last entry in ISO-8601 format e.g. 2007-12-03T10:15:30+01:00")
      .hasArg()
      .type(String.class)
      .build();

  private static final Option KEY_OPTION = Option
      .builder("k")
      .longOpt("key")
      .desc("Filter messages based on key")
      .hasArg()
      .type(String.class)
      .build();

  private static final Option FILTER_OPTION = Option
      .builder("f")
      .longOpt("filter")
      .desc(
          "Filter messages based on JSON path expression. " +
              "Must be in the format <json path>;<operation>;<value>. " +
              "Supported operations are equals, contains, startsWith and endsWith")
      .hasArgs()
      .type(String.class)
      .build();

  private static final Options options = new Options()
      .addOption(CONFIG_OPTION)
      .addOption(TOPIC_OPTION)
      .addOption(PARTITION_OPTION)
      .addOption(OUT_FILE_OPTION)
      .addOption(FROM_TIMESTAMP_OPTION)
      .addOption(TO_TIMESTAMP_OPTION)
      .addOption(KEY_OPTION)
      .addOption(FILTER_OPTION);

  private final CommandLine cmd;

  private ArgsConfig(final CommandLine cmd) {
    this.cmd = requireNonNull(cmd);
  }

  public static ArgsConfig load(final String[] args) {
    requireNonNull(args);

    try {
      return new ArgsConfig(parser.parse(options, args));
    } catch (ParseException e) {
      throw new AppException("Cannot parse command line arguments: " + e.getMessage(), e);
    }
  }

  public boolean hasConfig() {
    return cmd.getOptionValue(CONFIG_OPTION.getOpt()) != null;
  }

  public String config() {
    return cmd.getOptionValue(CONFIG_OPTION.getOpt());
  }

  public String topicName() {
    return cmd.getOptionValue(TOPIC_OPTION.getOpt());
  }

  public Optional<Integer> getPartitionId() {
    String option = cmd.getOptionValue(PARTITION_OPTION.getOpt());

    if(option == null) {
      return Optional.empty();
    }

    try {

      return Optional.of( Integer.parseInt(option));
    } catch (NumberFormatException e) {
      throw new AppException("cannot parse partition id from " + option);
    }
  }

  public String outputFilename() {
    return cmd.getOptionValue(OUT_FILE_OPTION.getOpt());
  }

  public boolean hasFromTimestamp() {
    return cmd.getOptionValue(FROM_TIMESTAMP_OPTION.getOpt()) != null;
  }

  public Optional<ZonedDateTime> fromTimestamp() {
    String value = cmd.getOptionValue(FROM_TIMESTAMP_OPTION.getOpt());

    if(value == null) {
      return Optional.empty();
    }

    return Optional.of(ZonedDateTime.parse(value));
  }

  public Optional<ZonedDateTime> toTimestamp() {
    String value = cmd.getOptionValue(TO_TIMESTAMP_OPTION.getOpt());

    if(value == null) {
      return Optional.empty();
    }

    return Optional.of(ZonedDateTime.parse(value));
  }

  public boolean hasKey() {
    return cmd.getOptionValue(KEY_OPTION.getOpt()) != null;
  }

  public String key() {
    return cmd.getOptionValue(KEY_OPTION.getOpt());
  }

  public boolean hasFilter() {
    String[] values = cmd.getOptionValues(FILTER_OPTION.getOpt());
    return values != null && values.length > 0;
  }

  public String[] filter() {
    return cmd.getOptionValues(FILTER_OPTION.getOpt());
  }

  public static void outputHelp() {
    var formatter = new HelpFormatter();
    formatter.setWidth(120);
    formatter.printHelp("kafka-data-extractor", options);
  }
}
