package org.krjura.kafka.kafkatools.dataextractor.kafka.filters;

import java.time.ZonedDateTime;

public final class FilterFactory {

  private FilterFactory() {
    // util
  }

  public static Filter allowAllFilter() {
    return new AllowAllFilter();
  }

  public static Filter toTimestampFilter(ZonedDateTime to) {
    if(to == null) {
      return new AllowAllFilter();
    } else {
      return new KeepBeforeTimestampFilter(to.toInstant().toEpochMilli());
    }
  }

}
