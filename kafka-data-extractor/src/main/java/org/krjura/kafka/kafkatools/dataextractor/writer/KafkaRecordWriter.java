package org.krjura.kafka.kafkatools.dataextractor.writer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.krjura.kafka.kafkatools.dataextractor.ex.AppException;

import static java.util.Objects.requireNonNull;

public final class KafkaRecordWriter implements AutoCloseable {

  private final String filename;
  private final Gson gson;
  private BufferedWriter bw;

  private KafkaRecordWriter(final String filename) {
    this.filename = requireNonNull(filename);
    this.gson = new GsonBuilder().setPrettyPrinting().create();

    open();
  }

  public static KafkaRecordWriter createKafkaRecordWriter(final String filename) {
    return new KafkaRecordWriter(filename);
  }

  private void open() {
    try {
      this.bw = new BufferedWriter(new FileWriter(filename));
    } catch (IOException e) {
      throw new AppException("Cannot open writer for file " + filename, e);
    }
  }

  public void writeRecord(final ConsumerRecord<String, String> record) {
    requireNonNull(record);

    var root = new JsonObject();

    root.addProperty("timestamp", record.timestamp());
    root.addProperty("topic", record.topic());
    root.addProperty("partition", record.partition());
    root.addProperty("offset", record.offset());
    root.addProperty("messageKey", record.key());
    root.add("messageValue", buildValueElement(record));

    writeString(root);
  }

  private JsonElement buildValueElement(final ConsumerRecord<String, String> record) {
    try {
      return JsonParser.parseString(record.value());
    } catch (JsonParseException e) {
      return new JsonPrimitive(record.value());
    }
  }

  private void writeString(final JsonObject root) {
    try {
      this.bw.write(gson.toJson(root));
      this.bw.write("\n");
    } catch (IOException e) {
      throw new AppException("cannot write string to the file " + this.filename, e);
    }
  }

  @Override
  public void close() throws Exception {
    if(this.bw != null) {
      this.bw.close();
    }
  }
}
