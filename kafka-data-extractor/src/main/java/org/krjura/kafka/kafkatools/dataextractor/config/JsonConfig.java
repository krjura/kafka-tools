package org.krjura.kafka.kafkatools.dataextractor.config;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.krjura.kafka.kafkatools.dataextractor.ex.AppException;

import static java.util.Objects.requireNonNull;

public final class JsonConfig {

  private static final String KAFKA_PROPS = "kafka.props";

  private final Map<String, Object> data;

  private JsonConfig(final Map<String, Object> data) {
    this.data = requireNonNull(data);
  }

  public static JsonConfig load(final String config) {
    var filename = config == null ? "config.json" : config;

    try(var reader = new FileReader(filename)) {
      var typeToken = TypeToken.getParameterized(Map.class, String.class, Object.class).getType();

      Map<String, Object> data = new Gson().fromJson(reader, typeToken);

      return new JsonConfig(data);
    } catch (IOException e) {
      throw new AppException("Cannot fetch configuration from " + filename, e);
    }
  }

  @SuppressWarnings("unchecked")
  public Map<String, Object> kafkaProps() {
    return (Map<String, Object>) this.data.getOrDefault(KAFKA_PROPS, new HashMap<>());
  }
}
