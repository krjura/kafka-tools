package org.krjura.kafka.kafkatools.dataextractor.kafka.filters;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public final class KeepBeforeTimestampFilter implements Filter {

  private final long timestamp;

  public KeepBeforeTimestampFilter(final long timestamp) {
    this.timestamp = timestamp;
  }

  @Override
  public boolean keep(final ConsumerRecord<String, String> record) {
    return record.timestamp() <= timestamp;
  }
}
