package org.krjura.kafka.kafkatools.dataextractor.kafka.filters;

import java.util.List;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import static java.util.Objects.requireNonNull;

public final class CompositeFilter implements Filter {

  private final List<Filter> filters;

  private CompositeFilter(final List<Filter> filters) {
    this.filters = requireNonNull(filters);
  }

  public static CompositeFilter compositeFilter(final List<Filter> filters) {
    return new CompositeFilter(filters);
  }

  @Override
  public boolean keep(final ConsumerRecord<String, String> record) {
    // if any filter return false drop record
    for(var filter : filters) {
      if(!filter.keep(record)) {
        return false;
      }
    }

    return true;
  }
}
