Kafka data extractor is a tool for extracting data from an Apache Kafka topic.
Application was built in Java and requires Java 11 to run.

It supports the following commands:

```
usage: kafka-data-extractor
 -c,--config <arg>      Location of the configuration file
 -f,--filter <arg>      Filter messages based on JSON path expression. Must be in the format <json
                        path>;<operation>;<value>. Supported operations are equals, contains, startsWith and endsWith
 -from,--from <arg>     Timestamp of the first entry in ISO-8601 format e.g. 2007-12-03T10:15:30+01:00
 -k,--key <arg>         Filter messages based on key
 -o,--output <arg>      Filename where to store the result
 -p,--partition <arg>   Id of the partition
 -t,--topic <arg>       Name of the topic
 -to,--to <arg>         Timestamp of the last entry in ISO-8601 format e.g. 2007-12-03T10:15:30+01:00
```

# Configuration

To set location of the configuration file use _--config_ flag. 
Application will expect configuration file in the following format:

```
{
  "kafka.props": {
    ....
  }
}
```

In the "kafka.props" element place any Kafka Consumer configuration. For example:

```
{
  "kafka.props": {
    "bootstrap.servers": "127.0.0.1:9092",
    "enable.auto.commit": false,
    "auto.offset.reset": "none",
    "max.poll.records": "100",
    "key.deserializer": "org.apache.kafka.common.serialization.StringDeserializer",
    "value.deserializer": "org.apache.kafka.common.serialization.StringDeserializer",
    "allow.auto.create.topics": false
  }
}
```

# Filtering

This tool can filter data from a Kafka topic based on timestamp, message key and JSON Path query string.
It is possible to combine _--key_, _--filter_, _--from_ and _--to_ in a single execution

## Filtering by key

To filter by Kafka message key use _--key_ flag. Search expects full match.

## Filtering by timestamp

To filter by timestamp use the _--from_ and _--to_ flag. To flag is optional.
Both flags expect timestamp in the ISO-8601 format e.g. "2020-03-01T10:00:00+01:00"

## Filtering by JSON Path.

You can also filter by JSON path query. 
This application uses [Jayway JsonPath](https://github.com/json-path/JsonPath) implementation.
Readme of the project provides documentation for building the query.

Filter must be defined in the following format <query>;<operation>;<value>
where:

* Query is a JSON path query string
* Operation can be equals, contains, startsWith and endsWith.
  * Equals compares the entire String. 
  * Contains compare withing a String.
  * StartsWith compare from the beginning of the String.
  * EndsWith compare from the end of the String.
* Value is what you expect.

If you specify multiple _filter_ flags, application will perform logical AND operation (both filters must match). 
 
# Examples
 
Extract all messages from a topic "examples" 
and save the result into file "/tmp/examples.json"
```
kafka-data-extractor --topic examples --output /tmp/examples.json 
```

Extract all messages from a topic "examples" 
and partition 0 
and save the result into file "/tmp/examples.json"
```
kafka-data-extractor --topic examples --partition 0 --output /tmp/examples.json 
```

Extract all messages from a topic "examples" 
with key "123456" 
and save the result into file "/tmp/examples.json"
```
kafka-data-extractor --topic examples --key 123456 --output /tmp/examples.json 
```

Extract all messages from a topic "examples" 
with timestamp greater then "2020-03-01T10:00:00+01:00" 
and save the result into file "/tmp/examples.json"
```
kafka-data-extractor --topic examples --from "2020-03-01T10:00:00+01:00" --output /tmp/examples.json 
```

Extract all messages from a topic "examples" with timestamp greater then "2020-03-01T10:00:00+01:00" 
and lower then "2020-03-01T10:00:00+01:00" 
and save the result into file "/tmp/examples.json"
```
kafka-data-extractor --topic examples --from "2020-03-01T10:00:00+01:00" --to "2020-03-01T11:00:00+01:00" --output /tmp/examples.json 
```

Extract all messages from a topic "examples"
with JSON body with "message" element in the root of the object that is equals to "testing"
and save the result into file "/tmp/examples.json"
```
kafka-data-extractor --topic examples --filter "$.message;equals;testing" --output /tmp/examples.json 
```

Extract all messages from a topic "examples"
with JSON body with "message" element in the root of the object that is equals to "testing"
and JSON body with "type" element in the root of the object that is equals to "account"
and save the result into file "/tmp/examples.json"
```
kafka-data-extractor --topic examples --filter "$.message;equals;testing" --filter "$.type;equals;account" --output /tmp/examples.json 
```